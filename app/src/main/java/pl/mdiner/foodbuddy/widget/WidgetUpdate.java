package pl.mdiner.foodbuddy.widget;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;

import java.util.Objects;

import static pl.mdiner.foodbuddy.utilities.Static.MARKER_DETAILS;
import static pl.mdiner.foodbuddy.utilities.Static.MARKER_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.WIDGET_ACTION;

public class WidgetUpdate extends IntentService {



    public WidgetUpdate() {
        super("FoodBuddy");
    }

    public static void startWidgetUpdate (Context context, String markerName, String markerSnippet){
        Intent i = new Intent(context, WidgetUpdate.class);
        i.putExtra(MARKER_NAME, markerName);
        i.putExtra(MARKER_DETAILS, markerSnippet);
        context.startService(i);
    }
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        assert intent != null;
        broadcastIntent(Objects.requireNonNull(intent.getExtras()).getString(MARKER_NAME),
                intent.getExtras().getString(MARKER_DETAILS));
    }

    private void broadcastIntent(String markerName, String recipeName) {
        Intent i = new Intent (WIDGET_ACTION);
        i.setAction(WIDGET_ACTION);
        i.putExtra(MARKER_NAME, markerName);
        i.putExtra(MARKER_DETAILS, recipeName);
        sendBroadcast(i);
    }
}
