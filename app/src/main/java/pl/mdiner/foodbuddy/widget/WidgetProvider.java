package pl.mdiner.foodbuddy.widget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.RemoteViews;

import java.util.ArrayList;
import java.util.Objects;

import pl.mdiner.foodbuddy.R;

import static pl.mdiner.foodbuddy.utilities.Static.MARKER_DETAILS;
import static pl.mdiner.foodbuddy.utilities.Static.MARKER_NAME;

public class WidgetProvider extends AppWidgetProvider {


    public static ArrayList<String> restaurantList;

    public static void updateAppWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId, String markerName, String markerDetails) {

        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.marker_widget);
        remoteViews.setTextViewText(R.id.widget_marker_name, markerName);
        remoteViews.setTextViewText(R.id.widget_marker_details, markerDetails);
        appWidgetManager.updateAppWidget(appWidgetId, remoteViews);
    }


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

    }

    public static void onStaticUpdate (Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds, String markerName, String markerDetails) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId, markerName, markerDetails);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
            String markerName = Objects.requireNonNull(intent.getExtras()).getString(MARKER_NAME);
            String markerDetails = intent.getExtras().getString(MARKER_DETAILS);
            if (!TextUtils.isEmpty(markerDetails)){
//                AppWidgetManager.getInstance(context).notifyAppWidgetViewDataChanged(AppWidgetManager.getInstance(context)
//                        .getAppWidgetIds(new ComponentName(context, WidgetProvider.class)), R.id.widget_list);

                onStaticUpdate(context, AppWidgetManager.getInstance(context),
                        AppWidgetManager.getInstance(context).getAppWidgetIds(new ComponentName(context, WidgetProvider.class))
                        , markerName, markerDetails);
                super.onReceive(context, intent);
            }
        }


    @Override
    public void onEnabled(Context context) {}

    @Override
    public void onDisabled(Context context) {}
}

