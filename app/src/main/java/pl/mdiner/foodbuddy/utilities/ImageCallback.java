package pl.mdiner.foodbuddy.utilities;

import android.widget.ProgressBar;

import com.squareup.picasso.Callback;

public class ImageCallback implements Callback {

    public final ProgressBar progressBar;

    protected ImageCallback(ProgressBar progressVar){
        progressBar = progressVar;
    }

    @Override
    public void onSuccess() {}
    @Override
    public void onError() {}
}
