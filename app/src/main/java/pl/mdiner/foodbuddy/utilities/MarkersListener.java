package pl.mdiner.foodbuddy.utilities;

/**
 * Created by Michał Diner on 25.07.2018.
 */
public interface MarkersListener {
    void onWidgetContentUpdate();
}
