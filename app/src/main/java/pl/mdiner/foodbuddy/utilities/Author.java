package pl.mdiner.foodbuddy.utilities;

/**
 * Created by Michał Diner on 25.07.2018.
 */
public class Author {

    String firstNme, lastName, email, phone, photo;

    public Author() {
    }

    public String getFirstName() {
        return firstNme;
    }

    public void setFirstName(String firstNme) {
        this.firstNme = firstNme;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
