package pl.mdiner.foodbuddy.utilities;

import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

public class Static {
    //Preferences
    public static final String FACEBOOK_FIRST_NAME = "FACEBOOK_FIRST_NAME";
    public static final String FACEBOOK_LAST_NAME = "FACEBOOK_LAST_NAME";
    public static final String FACEBOOK_PROFILE_PIC = "FACEBOOK_PROFILE_PIC";
    public static final String FACEBOOK_THUMB_PROFILE_PIC = "FACEBOOK_THUMB_PROFILE_PIC";
    public static final String EMAIL_FIRST_NAME = "EMAIL_FIRST_NAME";
    public static final String EMAIL_LAST_NAME ="EMAIL_LAST_NAME";
    public static final String EMAIL_PROFILE_PIC = "EMAIL_PROFILE_PIC";
    public static final String EMAIL_THUMB_PROFILE_PIC = "EMAIL_THUMB_PROFILE_PIC";
    public static final String FACEBOOK_BIRTHDAY = "FACEBOOK_BIRTHDAY";
    public static final String FACEBOOK_DESCRIPTION = "FACEBOOK_DESCRIPTION";
    public static final String EMAIL_BIRTHDAY = "EMAIL_BIRTHDAY";
    public static final String EMAIL_DESCRIPTION = "EMAIL_DESCRIPTION";
    public static final String FACEBOOK_AGE = "FACEBOOK_AGE";
    public static final String EMAIL_AGE = "EMAIL_AGE";
    public static final String EMAIL_DEVICE_TOKEN = "EMAIL_DEVICE_TOKEN";
    public static final String FACEBOOK_DEVICE_TOKEN = "FACEBOOK_DEVICE_TOKEN";

    //Firebase
    public static final String JPG = ".jpg";
    public static final String IMAGES = "images/";
    public static final String THUMB_IMAGES = "thumb_images/";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String PHOTO_URL = "photoUrl";
    public static final String THUMB_PHOTO_URL = "thumbPhotoUrl";
    public static final String USERS = "users";
    public static final String PHOTO = "photo";
    public static final String DESCRIPTION = "description";
    public static final String AGE = "age";
    public static final String FROM_SENDER_ID = "from_sender_id";
    public static final String ONLINE = "online";
    public static final String MESSAGES = "messages";
    public static final String MESSAGES_PICTURES = "messages_pictures";


    //Toast
    public static final String UPLOAD = "Uploading your picture...";
    public static final String UPLOAD_ERROR = "Failed to upload your picture.";

    //Error
    public static final String REQUIRED = "Required. ";
    public static final String PASSWORD_DONT_MATCH = "Passwords don't match";

    //Maps
    public static final String MARKER_NAME = "MARKER_NAME";
    public static final String MARKER_DETAILS = "MARKER_DETAILS";
    public static final LatLng mJadka = new LatLng(51.111975, 17.028549);
    public static final LatLng mPrzystan = new LatLng(51.114777, 17.032684);
    public static final LatLng mKonspira = new LatLng(51.109191, 17.028516);

    public static final CameraPosition WROCLAW = CameraPosition.builder()
            .target(new LatLng(51.107885 , 17.038538))
            .zoom(15)
            .build();

    // Chat
    public static final String FRAGMENT_TITLE_REQUESTS   = "Requests";
    public static final String FRAGMENT_TITLE_CHATS   = "Chats";
    public static final String FRAGMENT_TITLE_FRIENDS   = "Friends";
    public static final String VISIT_USER_ID = "visit_user_id";
    public static final String NOT_FRIENDS = "not_friends";
    public static final String REQUEST_SENT = "request_sent";
    public static final String FRIEND_REQUEST = "friendRequest";
    public static final String REQUEST_TYPE = "requestType";
    public static final String SENT = "sent";
    public static final String RECEIVED = "received";
    public static final String CANCEL_FRIEND_REQUEST = "Cancel friend request";
    public static final String SEND_FRIEND_REQUEST = "Send friend request";
    public static final String REQUEST_RECEIVED = "request_received";
    public static final String FRIENDS = "friends";
    public static final String UNFRIEND = "Unfriend";
    public static final String ACCEPT_FRIEND_REQUEST = "Accept friend request";
    public static final String NOTIFICATIONS = "notifications";
    public static final String FROM = "from";
    public static final String TYPE = "type";
    public static final String REQUEST = "request";
    public static final String DATE = "date";
    public static final String FRIENDS_SINCE = "Friends since: ";
    public static final String FULL_NAME = "full_name";
    public static final String TRUE = "true";
    public static final String MESSAGE = "messages";
    public static final String SEEN = "seen";
    public static final String TEXT = "text";
    public static final String TIME = "time";
    public static final String USER_STATUS = "description";
    public static final String IMAGE = "image";
    public static final String LOG_OUT = "LOG_OUT";
    public static final String ONLINE_USER_ID = "online_user_id";

    // Images
    public static final int PICK_IMAGE_REQUEST = 71;

    //Welcome activity
    public static final String IS_LOGGED = "IS_LOGGED";
    public static final int FIRST_PAGE = 0;
    public static final int SECOND_PAGE = 1;
    public static final int THIRD_PAGE = 2;

    //Widget
    public static final String WIDGET_ACTION = "android.appwidget.action.APPWIDGET_UPDATE";

    //Authors
    public static final String JSON_URL = "https://randomuser.me/api/?inc=gender,name,nat,phone,email,picture&results=3";

    //Misc
    public static final String HOURS_AGO = " hours ago";
    private static final int SECOND = 1000;
    public static final int MINUTE = 60 * SECOND;
    public static final int HOUR = 60 * MINUTE;
    public static final int DAY_MILLIS = 24 * HOUR;
    public static final String ACTIVE_FEW_SECONDS_AGO = "Active few seconds ago.";
    public static final String ACTIVE_A_MINUTE_AGO = "Active a minute ago.";
    public static final String MINUTES_AGO = " minutes ago";
    public static final String ACTIVE_AN_HOUR_AGO = "Active an hour ago.";
    public static final String ACTIVE_YESTERDAY = "Active yesterday.";
    public static final String DAYS_AGO_HAVE_BEEN_ACTIVE = " days ago have been active.";
}
