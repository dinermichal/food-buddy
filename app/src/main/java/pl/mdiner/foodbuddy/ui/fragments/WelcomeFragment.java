package pl.mdiner.foodbuddy.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import pl.mdiner.foodbuddy.R;
import pl.mdiner.foodbuddy.adapters.WelcomeImageAdapter;
import pl.mdiner.foodbuddy.ui.activities.CreateUserActivity;
import pl.mdiner.foodbuddy.ui.activities.LogInActivity;

public class WelcomeFragment extends Fragment {

    @BindView(R.id.vpPager)
    ViewPager mPager;
    @BindView(R.id.tabDots)
    TabLayout mIndicatorDots;

    private Unbinder unbinder;

    public WelcomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welcome, container, false);
        unbinder = ButterKnife.bind(this, view);
        WelcomeImageAdapter mPagerAdapter = new WelcomeImageAdapter(getChildFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mIndicatorDots.setupWithViewPager(mPager, true);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.signIn)
    public void signIn(){
        startSpecificActivity(LogInActivity.class);
    }

    @OnClick(R.id.signUp)
    public void signUp(){
        startSpecificActivity(CreateUserActivity.class);
    }

    public void startSpecificActivity(Class<?> otherActivityClass) {
        Intent intent = new Intent(getContext(), otherActivityClass);
        startActivity(intent);
    }
}
