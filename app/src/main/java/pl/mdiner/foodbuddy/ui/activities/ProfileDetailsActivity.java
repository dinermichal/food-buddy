package pl.mdiner.foodbuddy.ui.activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.zelory.compressor.Compressor;
import pl.mdiner.foodbuddy.R;
import pl.mdiner.foodbuddy.firebase.FirebaseUser;
import pl.mdiner.foodbuddy.utilities.FacebookUtils;
import pl.mdiner.foodbuddy.utilities.ImageCallback;
import pl.mdiner.foodbuddy.utilities.Utils;

import static android.text.TextUtils.isEmpty;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_AGE;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_BIRTHDAY;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_DESCRIPTION;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_DEVICE_TOKEN;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_FIRST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_LAST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_PROFILE_PIC;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_THUMB_PROFILE_PIC;
import static pl.mdiner.foodbuddy.utilities.Static.FACEBOOK_AGE;
import static pl.mdiner.foodbuddy.utilities.Static.FACEBOOK_BIRTHDAY;
import static pl.mdiner.foodbuddy.utilities.Static.FACEBOOK_DESCRIPTION;
import static pl.mdiner.foodbuddy.utilities.Static.FACEBOOK_DEVICE_TOKEN;
import static pl.mdiner.foodbuddy.utilities.Static.FACEBOOK_FIRST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.FACEBOOK_LAST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.FACEBOOK_PROFILE_PIC;
import static pl.mdiner.foodbuddy.utilities.Static.FACEBOOK_THUMB_PROFILE_PIC;
import static pl.mdiner.foodbuddy.utilities.Static.FIRST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.IMAGES;
import static pl.mdiner.foodbuddy.utilities.Static.JPG;
import static pl.mdiner.foodbuddy.utilities.Static.LAST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.PICK_IMAGE_REQUEST;
import static pl.mdiner.foodbuddy.utilities.Static.REQUIRED;
import static pl.mdiner.foodbuddy.utilities.Static.THUMB_IMAGES;
import static pl.mdiner.foodbuddy.utilities.Static.THUMB_PHOTO_URL;
import static pl.mdiner.foodbuddy.utilities.Static.UPLOAD;
import static pl.mdiner.foodbuddy.utilities.Static.UPLOAD_ERROR;
import static pl.mdiner.foodbuddy.utilities.Static.USERS;
import static pl.mdiner.foodbuddy.utilities.Utils.setPreferences;

public class ProfileDetailsActivity extends AppCompatActivity {

    //Networking
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private LoginManager mAuthFacebook;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private DatabaseReference mDatabase;
    private String profileImageLink;
    private Bitmap thumb_bitmap;
    private StorageReference thumbPhotoUrlReference;
    private String onlineUserId;

    private Unbinder unbinder;
    @BindView(R.id.ivProfilePicDetails)
    ImageView ivProfileImage;
    @BindView(R.id.full_name)
    TextView tvFullName;
    @BindView(R.id.tvBirthday)
    TextView birthdayEt;
    @BindView(R.id.etDescription)
    TextView tvDescription;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    private String firstName;
    private String lastName;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent listIntent = new Intent(ProfileDetailsActivity.this, MainActivity.class);
        startActivity(listIntent);
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_details);
        unbinder = ButterKnife.bind(this);
        mAuthFacebook = LoginManager.getInstance();
        progressBar.setVisibility(View.VISIBLE);
        // Firebase Database
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.keepSynced(true);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        mAuth = FirebaseAuth.getInstance();
        onlineUserId = mAuth.getCurrentUser().getUid();
        initializeAuth();

        final ProgressBar finalProgressBar = progressBar;
        if (onlineUserId != null){
            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.child(USERS).child(onlineUserId).child(THUMB_PHOTO_URL).exists() ) {
                        profileImageLink = dataSnapshot.child(USERS).child(onlineUserId).child(THUMB_PHOTO_URL).getValue().toString();
                        firstName = Utils.getPreferences(FACEBOOK_FIRST_NAME, ProfileDetailsActivity.this);
                        lastName = Utils.getPreferences(FACEBOOK_LAST_NAME,ProfileDetailsActivity.this);
                        if (isEmpty(firstName) && isEmpty(lastName)){
                        firstName = dataSnapshot.child(USERS).child(onlineUserId).child(FIRST_NAME).getValue().toString();
                        lastName =  dataSnapshot.child(USERS).child(onlineUserId).child(LAST_NAME).getValue().toString();
                            if (FacebookUtils.isLoggedIn()){
                                setPreferences(FACEBOOK_FIRST_NAME, firstName, ProfileDetailsActivity.this);
                                setPreferences(FACEBOOK_LAST_NAME, lastName, ProfileDetailsActivity.this);
                            }
                            else {
                                setPreferences(EMAIL_FIRST_NAME, firstName, ProfileDetailsActivity.this);
                                setPreferences(EMAIL_LAST_NAME, lastName, ProfileDetailsActivity.this);
                            }
                        }
                        Picasso.with(ProfileDetailsActivity.this).load(profileImageLink).networkPolicy(NetworkPolicy.OFFLINE)
                                .into(ivProfileImage, new Callback() {
                                    @Override
                                    public void onSuccess(){}
                                    @Override
                                    public void onError(){
                                        Picasso.with(ProfileDetailsActivity.this).load(profileImageLink).into(ivProfileImage,
                                                new ImageCallback(finalProgressBar){
                                                    @Override
                                                    public void onSuccess(){
                                                        if (this.progressBar != null){
                                                            this.progressBar.setVisibility(View.GONE);
                                                        }
                                                    }

                                                    @Override
                                                    public void onError() {
                                                        super.onError();
                                                        if (this.progressBar != null){
                                                            this.progressBar.setVisibility(View.GONE);
                                                            Picasso.with(ProfileDetailsActivity.this).load(R.drawable.default_profile).into(ivProfileImage);
                                                        }
                                                    }
                                                });
                                    }
                                });
                    }
                    else{
                        if (FacebookUtils.isLoggedIn()){
                            profileImageLink= Utils.getPreferences(FACEBOOK_PROFILE_PIC, ProfileDetailsActivity.this);
                            Picasso.with(ProfileDetailsActivity.this).load(profileImageLink).networkPolicy(NetworkPolicy.OFFLINE)
                                    .into(ivProfileImage, new Callback(){
                                        @Override
                                        public void onSuccess(){}
                                        @Override
                                        public void onError() {
                                            Picasso.with(ProfileDetailsActivity.this).load(profileImageLink).into(ivProfileImage,
                                                    new ImageCallback(finalProgressBar){
                                                        @Override
                                                        public void onSuccess(){
                                                            if (this.progressBar != null){
                                                                this.progressBar.setVisibility(View.GONE);
                                                            }
                                                        }

                                                        @Override
                                                        public void onError() {
                                                            super.onError();
                                                            if (this.progressBar != null){
                                                                this.progressBar.setVisibility(View.GONE);
                                                                Picasso.with(ProfileDetailsActivity.this).load(R.drawable.default_profile).into(ivProfileImage);
                                                            }
                                                        }
                                                    });
                                        }
                                    });
                        }
                        else{
                            profileImageLink = Utils.getPreferences(EMAIL_PROFILE_PIC, ProfileDetailsActivity.this);
                            if (isEmpty(profileImageLink)){
                                Picasso.with(ProfileDetailsActivity.this).load(R.drawable.default_profile).into(ivProfileImage);
                            }
                            else{
                                Picasso.with(ProfileDetailsActivity.this).load(profileImageLink).networkPolicy(NetworkPolicy.OFFLINE)
                                        .into(ivProfileImage, new Callback(){
                                            @Override
                                            public void onSuccess(){}
                                            @Override
                                            public void onError(){
                                                Picasso.with(ProfileDetailsActivity.this).load(profileImageLink).into(ivProfileImage,
                                                        new ImageCallback(finalProgressBar){
                                                            @Override
                                                            public void onSuccess(){
                                                                if (this.progressBar != null){
                                                                    this.progressBar.setVisibility(View.GONE);
                                                                }
                                                            }

                                                            @Override
                                                            public void onError() {
                                                                super.onError();
                                                                if (this.progressBar != null){
                                                                    this.progressBar.setVisibility(View.GONE);
                                                                    Picasso.with(ProfileDetailsActivity.this).load(R.drawable.default_profile).into(ivProfileImage);
                                                                }
                                                            }
                                                        });
                                            }
                                        });
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError){}
            });
        }

    }



    private void initializeAuth(){
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                if (mAuth.getCurrentUser() != null){
                    if (FacebookUtils.isLoggedIn()){

                        tvFullName.setText(Utils.getPreferences(FACEBOOK_FIRST_NAME,ProfileDetailsActivity.this)
                                + " "
                                + Utils.getPreferences(FACEBOOK_LAST_NAME,
                                ProfileDetailsActivity.this));
                        //Email
                        }
                        else{
                        tvFullName.setText(Utils.getPreferences(EMAIL_FIRST_NAME,ProfileDetailsActivity.this)
                                + " "
                                + Utils.getPreferences(EMAIL_LAST_NAME,
                                ProfileDetailsActivity.this));
                        }
                }
               else{
                    Intent i = new Intent(ProfileDetailsActivity.this, WelcomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
            }
        };
    }

    private void setBirthday(){
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener(){

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                if(FacebookUtils.isLoggedIn()){
                    setPreferences(FACEBOOK_AGE,  Utils.getAge(year,monthOfYear, dayOfMonth),
                            ProfileDetailsActivity.this);
                }
                else{
                    setPreferences(EMAIL_AGE,  Utils.getAge(year,monthOfYear, dayOfMonth),
                        ProfileDetailsActivity.this);
                }
                String myFormat = "dd/MM/yy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.GERMAN);

                birthdayEt.setText(sdf.format(myCalendar.getTime()));
            }
        };
        birthdayEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 new DatePickerDialog(ProfileDetailsActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void saveDetails(){
        validateForm();
        String birthday = birthdayEt.getText().toString().trim();
        String description = tvDescription.getText().toString().trim();
        // Facebook
        if (FacebookUtils.isLoggedIn()){
            writeInfoToDatabaseFacebook(birthday, description);
        }
        // Email
       else{
            writeInfoToDatabaseEmail(birthday, description);
        }
    }

    private void writeInfoToDatabaseFacebook(String birthday, String description){
        String firstName = Utils.getPreferences(FACEBOOK_FIRST_NAME, ProfileDetailsActivity.this);
        String lastName = Utils.getPreferences(FACEBOOK_LAST_NAME, ProfileDetailsActivity.this);
        String photoUrl = Utils.getPreferences(FACEBOOK_PROFILE_PIC, ProfileDetailsActivity.this);
        String thumbPhotoUrl = Utils.getPreferences(FACEBOOK_THUMB_PROFILE_PIC, ProfileDetailsActivity.this);
        String age = Utils.getPreferences(FACEBOOK_AGE, ProfileDetailsActivity.this);
        String deviceToken = Utils.getPreferences(FACEBOOK_DEVICE_TOKEN, ProfileDetailsActivity.this);
        setPreferences(FACEBOOK_BIRTHDAY, birthday, ProfileDetailsActivity.this);
        setPreferences(FACEBOOK_DESCRIPTION, description, ProfileDetailsActivity.this);
        FirebaseUser firebaseUser = new FirebaseUser(firstName,lastName, photoUrl, thumbPhotoUrl, birthday, description, age, deviceToken);
        mDatabase.child(USERS).child(mAuth.getCurrentUser().getUid()).setValue(firebaseUser);
    }

    private void writeInfoToDatabaseEmail(String birthday, String description){
        String firstName = Utils.getPreferences(EMAIL_FIRST_NAME, ProfileDetailsActivity.this);
        String lastName = Utils.getPreferences(EMAIL_LAST_NAME, ProfileDetailsActivity.this);
        String photoUrl = Utils.getPreferences(EMAIL_PROFILE_PIC, ProfileDetailsActivity.this);
        String thumbPhotoUrl = Utils.getPreferences(EMAIL_THUMB_PROFILE_PIC, ProfileDetailsActivity.this);
        String deviceToken = Utils.getPreferences(EMAIL_DEVICE_TOKEN, ProfileDetailsActivity.this);
        String age = Utils.getPreferences(EMAIL_AGE, ProfileDetailsActivity.this);
        setPreferences(EMAIL_BIRTHDAY, birthday, ProfileDetailsActivity.this);
        setPreferences(EMAIL_DESCRIPTION, description, ProfileDetailsActivity.this);
        FirebaseUser firebaseUser = new FirebaseUser(firstName,lastName, photoUrl, thumbPhotoUrl, birthday, description, age, deviceToken);
        mDatabase.child(USERS).child(mAuth.getCurrentUser().getUid()).setValue(firebaseUser);
    }

    private void writePhotoToPreferencesFacebook(String photoUrl){
        setPreferences(FACEBOOK_PROFILE_PIC, photoUrl, ProfileDetailsActivity.this);
    }

    private void writePhotoToDatabaseEmail(String photoUrl){
        setPreferences(EMAIL_PROFILE_PIC, photoUrl, ProfileDetailsActivity.this);
    }
    private void chooseImage(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null ){
            Uri filePath = data.getData();
            CropImage.activity(filePath)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);
        }
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK){
                    Uri resultUri = result.getUri();
                    final File thumb_filePathUri = new File(resultUri.getPath());
                    try{
                            thumb_bitmap = new Compressor(this)
                                    .setMaxWidth(200)
                                    .setMaxHeight(200)
                                    .setQuality(50)
                                    .compressToBitmap(thumb_filePathUri);
                            Picasso.with(ProfileDetailsActivity.this)
                                .load(MediaStore.Images.Media.insertImage(this.getContentResolver(), thumb_bitmap, "Title", null))
                                .into(ivProfileImage);
                            showProgressDialog();

                       }
                    catch (IOException e){
                        e.printStackTrace();
                      }
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    thumb_bitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                    final byte[] thumb_byte = byteArrayOutputStream.toByteArray();
                    final StorageReference photoUrlReference = storageReference.child
                            (IMAGES + mAuth.getCurrentUser().getUid() + JPG);
                    thumbPhotoUrlReference = storageReference.child
                            (THUMB_IMAGES + mAuth.getCurrentUser().getUid()+ JPG);

                    photoUrlReference.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onComplete(@NonNull final Task<UploadTask.TaskSnapshot> task) {
                            if(task.isSuccessful()){
                                UploadTask uploadTask = thumbPhotoUrlReference.putBytes(thumb_byte);
                                uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> thumbTaskResult) {
//                                        String thumbDownloadUrl = thumbTaskResult.getResult().getUploadSessionUri().toString();
                                        if (task.isSuccessful()) {
                                            thumbPhotoUrlReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                @Override
                                                public void onSuccess(Uri uri) {
                                                    if (FacebookUtils.isLoggedIn()) {
                                                        setPreferences(FACEBOOK_THUMB_PROFILE_PIC, String.valueOf(uri), ProfileDetailsActivity.this);
                                                    } else {
                                                        setPreferences(EMAIL_THUMB_PROFILE_PIC, String.valueOf(uri), ProfileDetailsActivity.this);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                                Context context = getApplicationContext();
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, UPLOAD, duration);
                                toast.show();

                                photoUrlReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        if (FacebookUtils.isLoggedIn()){
                                            writePhotoToPreferencesFacebook(String.valueOf(uri));
                                        }
                                        else{
                                            writePhotoToDatabaseEmail(String.valueOf(uri));
                                        }
                                    }
                                });
                            }
                            else{
                                Context context = getApplicationContext();
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, UPLOAD_ERROR, duration);
                                toast.show();
                            }
                            hideProgressDialog();
                        }
                    });
               }
               else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
                    Exception error = result.getError();
               }
            }
    }

    private void validateForm(){
        boolean valid = true;

        String birthday = birthdayEt.getText().toString();
        if (isEmpty(birthday)){
            birthdayEt.setError(REQUIRED);
            valid = false;
        }
        else{
            birthdayEt.setError(null);
        }
        if (valid){
        Intent i = new Intent(ProfileDetailsActivity.this, MainActivity.class);
        startActivity(i);
        }
    }
    @Override
    protected void onStart(){
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop(){
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    @Override
    public void onResume(){
        super.onResume();
        initializeAuth();
    }

    @Override
    public void onRestart(){
        super.onRestart();
        initializeAuth();
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        if (mAuthListener != null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
        unbinder.unbind();
    }

    @OnClick (R.id.btnChooseImage)
    public void choose(){
        chooseImage();
    }
    @OnClick(R.id.btnSaveDetails)
    public void save(){
        saveDetails();
    }
    @OnClick(R.id.tvBirthday)
    public void birthday(){
        setBirthday();
    }
    @OnClick(R.id.btnCancel)
    public void cancel(){
        Intent intent = new Intent(ProfileDetailsActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @VisibleForTesting
    private ProgressDialog mProgressDialog;

    protected void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }


    protected void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
