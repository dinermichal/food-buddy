package pl.mdiner.foodbuddy.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import pl.mdiner.foodbuddy.R;
import pl.mdiner.foodbuddy.utilities.FacebookUtils;

import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_DEVICE_TOKEN;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_FIRST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_LAST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.PASSWORD_DONT_MATCH;
import static pl.mdiner.foodbuddy.utilities.Static.REQUIRED;
import static pl.mdiner.foodbuddy.utilities.Static.USERS;
import static pl.mdiner.foodbuddy.utilities.Utils.setPreferences;

public class CreateUserActivity extends FacebookUtils {

    private static final String TAG = CreateUserActivity.class.getSimpleName();
    private FirebaseAuth mAuth;
    @BindView(R.id.etEmail)
    EditText mEmailField;
    @BindView(R.id.etFirstName)
    EditText mFirstName;
    @BindView(R.id.etPassword)
    EditText mPasswordField;
    @BindView(R.id.etConfirmPassword)
    EditText mConfirmPasswordField;
    @BindView(R.id.etLastName)
    EditText mLastName;
    @BindView(R.id.button_facebook_login)
    LoginButton loginButton;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_user);
        unbinder = ButterKnife.bind(this);

        LoginManager mAuthFacebook = LoginManager.getInstance();
        mAuth = FirebaseAuth.getInstance();
        DatabaseReference userReference = FirebaseDatabase.getInstance().getReference().child(USERS);
        initializeFacebookLogin(CreateUserActivity.this);
        createAuthStateListener();
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    private void writeBasicInfoToPreferencesEmail(String firstName, String lastName, String id) {
        setPreferences(EMAIL_FIRST_NAME, firstName, CreateUserActivity.this);
        setPreferences(EMAIL_LAST_NAME, lastName, CreateUserActivity.this);
    }

    private void createAccount(String email, String password){
        Log.d(TAG, "createAccount:" + email);
        if (!validateForm()){
            return;
        }

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener
                (this, new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                        if (task.isSuccessful()){
                            String deviceToken = FirebaseInstanceId.getInstance().getToken();
                            setPreferences(EMAIL_DEVICE_TOKEN, deviceToken, CreateUserActivity.this);
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        }

                        else{
                            Toast.makeText(CreateUserActivity.this, task.getException().toString(), Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                    }
                });
    }

    private void createAuthStateListener() {
        FirebaseAuth.AuthStateListener mAuthListener = new FirebaseAuth.AuthStateListener() {

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Intent intent = new Intent(CreateUserActivity.this, ProfileDetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }

        };
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.btnSignUp)
    public void signUp(){
        createAccount(mEmailField.getText().toString().trim().toLowerCase(), mPasswordField.getText().toString().trim());
    }
    @OnClick(R.id.tvLogin)
    public void login(){
        startSpecificActivity(LogInActivity.class);
    }

    public void startSpecificActivity(Class<?> otherActivityClass) {
        Intent intent = new Intent(this, otherActivityClass);
        startActivity(intent);
    }
    private boolean validateForm() {
        boolean valid = true;

        String fName = mFirstName.getText().toString();
        if (TextUtils.isEmpty(fName)) {
            mFirstName.setError(REQUIRED);
            mFirstName.requestFocus();
            valid = false;
        } else {
            mFirstName.setError(null);
        }
        String lName = mLastName.getText().toString();
        if (TextUtils.isEmpty(lName)) {
            mLastName.setError(REQUIRED);
            mLastName.requestFocus();
            valid = false;
        } else {
            mLastName.setError(null);
        }
        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError(REQUIRED);
            mLastName.requestFocus();
            valid = false;
        } else {
            mEmailField.setError(null);
        }

        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError(REQUIRED);
            mLastName.requestFocus();
            valid = false;
        } else {
            mPasswordField.setError(null);
        }

        String confirmPassword = mConfirmPasswordField.getText().toString();
        if (password.equals(confirmPassword)) {
            mPasswordField.setError(null);
            mLastName.requestFocus();
        } else {
            mPasswordField.setError(PASSWORD_DONT_MATCH);
            valid = false;
        }

        return valid;
    }

    private void sendEmailVerification() {
        final FirebaseUser user = mAuth.getCurrentUser();
        user.sendEmailVerification()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(CreateUserActivity.this,
                                    "Verification email sent to " + user.getEmail(),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "sendEmailVerification", task.getException());
                            Toast.makeText(CreateUserActivity.this,
                                    "Failed to send verification email.",
                                    Toast.LENGTH_SHORT).show();
                        }
                      }
                });
        }

    private void updateUI(FirebaseUser user) {
        hideProgressDialog();
        if (user != null) {
            writeBasicInfoToPreferencesEmail(mFirstName.getText().toString(), mLastName.getText().toString(), mAuth.getCurrentUser().getUid());
            Intent createUser = new Intent(CreateUserActivity.this, ProfileDetailsActivity.class);
            createUser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            sendEmailVerification();
            startActivity(createUser);
        }
    }
}