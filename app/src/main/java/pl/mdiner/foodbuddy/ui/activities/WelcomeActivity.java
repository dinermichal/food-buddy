package pl.mdiner.foodbuddy.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import pl.mdiner.foodbuddy.R;
import pl.mdiner.foodbuddy.maps.InfoWindowDetails;
import pl.mdiner.foodbuddy.ui.fragments.WelcomeFragment;
import pl.mdiner.foodbuddy.utilities.MarkersListener;
import pl.mdiner.foodbuddy.utilities.Utils;

import static android.text.TextUtils.isEmpty;
import static pl.mdiner.foodbuddy.utilities.Static.LOG_OUT;
import static pl.mdiner.foodbuddy.utilities.Static.MARKER_DETAILS;
import static pl.mdiner.foodbuddy.utilities.Static.MARKER_NAME;
import static pl.mdiner.foodbuddy.utilities.Utils.clearPreferences;
import static pl.mdiner.foodbuddy.widget.WidgetUpdate.startWidgetUpdate;

public class WelcomeActivity extends AppCompatActivity implements MarkersListener{
    private Unbinder unbinder;
    private boolean isLoggedOut;
    private FirebaseAuth mAuth;
    private LoginManager mAuthFacebook;
    private String toMarkers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.container);
        unbinder = ButterKnife.bind(this);
        mAuth= FirebaseAuth.getInstance();
        mAuthFacebook = LoginManager.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Bundle getWhereTo = getIntent().getExtras();
        if (getWhereTo != null){
            isLoggedOut = getWhereTo.getBoolean(LOG_OUT, false);
            toMarkers = getWhereTo.getString(MARKER_NAME);
        }
        if (isLoggedOut){
            mAuth.signOut();
            mAuthFacebook.logOut();
            clearPreferences(WelcomeActivity.this);
        }

        if (savedInstanceState == null) {
            // Checks if user is logged.
            if (isEmpty(toMarkers)){
                if (user == null) {
                    Fragment fragment = new WelcomeFragment();
                    openFragment(fragment);
                }
                // Author is logged in.
                else{
                    Intent sendToMain = new Intent(WelcomeActivity.this, MainActivity.class);
                    startActivity(sendToMain);
                }

            }
            else{
                InfoWindowDetails infoWindowDetails = new InfoWindowDetails();
                Bundle bundle = new Bundle();
                bundle.putString(MARKER_NAME, toMarkers);
                infoWindowDetails.setArguments(bundle);
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                               .add(R.id.fragment_container, infoWindowDetails)
                               .commit();
            }
        }
    }


    public void openFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onWidgetContentUpdate() {
        updateWidget(this);
    }
    public static void updateWidget(Context context) {
        String markerName = Utils.getPreferences(MARKER_NAME, context);
        String markerSnippet = Utils.getPreferences(MARKER_DETAILS, context);
        startWidgetUpdate(context, markerName, markerSnippet);
    }
}
