package pl.mdiner.foodbuddy.ui.activities;

import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import pl.mdiner.foodbuddy.R;
import pl.mdiner.foodbuddy.utilities.ImageCallback;

import static pl.mdiner.foodbuddy.utilities.Static.ACCEPT_FRIEND_REQUEST;
import static pl.mdiner.foodbuddy.utilities.Static.AGE;
import static pl.mdiner.foodbuddy.utilities.Static.CANCEL_FRIEND_REQUEST;
import static pl.mdiner.foodbuddy.utilities.Static.DATE;
import static pl.mdiner.foodbuddy.utilities.Static.DESCRIPTION;
import static pl.mdiner.foodbuddy.utilities.Static.FIRST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.FRIENDS;
import static pl.mdiner.foodbuddy.utilities.Static.FRIEND_REQUEST;
import static pl.mdiner.foodbuddy.utilities.Static.FROM;
import static pl.mdiner.foodbuddy.utilities.Static.LAST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.NOTIFICATIONS;
import static pl.mdiner.foodbuddy.utilities.Static.NOT_FRIENDS;
import static pl.mdiner.foodbuddy.utilities.Static.PHOTO_URL;
import static pl.mdiner.foodbuddy.utilities.Static.RECEIVED;
import static pl.mdiner.foodbuddy.utilities.Static.REQUEST;
import static pl.mdiner.foodbuddy.utilities.Static.REQUEST_RECEIVED;
import static pl.mdiner.foodbuddy.utilities.Static.REQUEST_SENT;
import static pl.mdiner.foodbuddy.utilities.Static.REQUEST_TYPE;
import static pl.mdiner.foodbuddy.utilities.Static.SEND_FRIEND_REQUEST;
import static pl.mdiner.foodbuddy.utilities.Static.SENT;
import static pl.mdiner.foodbuddy.utilities.Static.TYPE;
import static pl.mdiner.foodbuddy.utilities.Static.UNFRIEND;
import static pl.mdiner.foodbuddy.utilities.Static.USERS;
import static pl.mdiner.foodbuddy.utilities.Static.VISIT_USER_ID;


public class AllUsersDetailsActivity extends AppCompatActivity{

    private String firstName;
    private String lastName;
    private String name;
    private String description;
    private String age;
    private String photoUrl;
    private String CURRENT_STATE;
    private DatabaseReference friendRequestReference;
    private String senderUserId;
    private String receiverUserId;
    private DatabaseReference friendsReference;
    private DatabaseReference notificationsReference;

    @BindView(R.id.profile_visit_user_decline_request)
    Button declineFriendRequestBtn;
    @BindView(R.id.profile_visit_user_friend_request)
    Button sendFriendRequestBtn;
    @BindView(R.id.profile_visit_user_name)
    TextView profileName;
    @BindView(R.id.profile_visit_user_age)
    TextView profileAge;
    @BindView(R.id.profile_visit_user_description)
    TextView profileDescription;
    @BindView(R.id.profile_visit_user_image)
    ImageView profilePhoto;
    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_all_users_details);
        unbinder = ButterKnife.bind(this);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        receiverUserId = getIntent().getExtras().get(VISIT_USER_ID).toString();
        senderUserId = mAuth.getCurrentUser().getUid();
        DatabaseReference userReference = FirebaseDatabase.getInstance().getReference().child(USERS).child(receiverUserId);
        userReference.keepSynced(true);
        friendRequestReference = FirebaseDatabase.getInstance().getReference().child(FRIEND_REQUEST);
        friendRequestReference.keepSynced(true);
        friendsReference = FirebaseDatabase.getInstance().getReference().child(FRIENDS);
        friendsReference.keepSynced(true);
        notificationsReference = FirebaseDatabase.getInstance().getReference().child(NOTIFICATIONS);
        notificationsReference.keepSynced(true);

        CURRENT_STATE = NOT_FRIENDS;
        declineFriendRequestBtn.setVisibility(View.GONE);
        declineFriendRequestBtn.setEnabled(false);

        if(senderUserId.equals(receiverUserId)){
            sendFriendRequestBtn.setVisibility(View.GONE);
            declineFriendRequestBtn.setVisibility(View.GONE);
        }
        else{
            sendFriendRequestBtn.setVisibility(View.VISIBLE);
            declineFriendRequestBtn.setVisibility(View.VISIBLE);
        }


        ProgressBar progressBar;
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        final ProgressBar finalProgressBar1 = progressBar;
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null){
            userReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot){
                    if(dataSnapshot.exists()){
                        firstName = dataSnapshot.child(FIRST_NAME).getValue().toString();
                        lastName = dataSnapshot.child(LAST_NAME).getValue().toString();
                        name = firstName + " " + lastName;
                        description = dataSnapshot.child(DESCRIPTION).getValue().toString();
                        age = dataSnapshot.child(AGE).getValue().toString();
                        photoUrl = dataSnapshot.child(PHOTO_URL).getValue().toString();

                        profileName.setText(name);
                        profileAge.setText(age);
                        profileDescription.setText(description);
                        Picasso.with(AllUsersDetailsActivity.this).load(photoUrl).into(profilePhoto,
                                new ImageCallback(finalProgressBar1) {
                                    @Override
                                    public void onSuccess() {
                                        if (this.progressBar != null) {
                                            this.progressBar.setVisibility(View.GONE);
                                        }
                                    }
                                });
                        friendRequestReference.child(senderUserId).
                                addListenerForSingleValueEvent(new ValueEventListener(){
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot){
                                        if (dataSnapshot.hasChild(receiverUserId)){
                                            String reqType = dataSnapshot.child(receiverUserId).child(REQUEST_TYPE).getValue().toString();
                                            if (reqType.equals(SENT)){
                                                CURRENT_STATE = REQUEST_SENT;
                                                sendFriendRequestBtn = findViewById(R.id.profile_visit_user_friend_request);
                                                sendFriendRequestBtn.setText(CANCEL_FRIEND_REQUEST);
                                                declineFriendRequestBtn.setVisibility(View.GONE);
                                                declineFriendRequestBtn.setEnabled(false);

                                            } else if (reqType.equals(RECEIVED)){
                                                CURRENT_STATE = REQUEST_RECEIVED;
                                                sendFriendRequestBtn = findViewById(R.id.profile_visit_user_friend_request);
                                                sendFriendRequestBtn.setText(ACCEPT_FRIEND_REQUEST);

                                                declineFriendRequestBtn.setVisibility(View.VISIBLE);
                                                declineFriendRequestBtn.setEnabled(true);
                                            }
                                        }
                                        else{
                                            friendsReference.child(senderUserId)
                                                    .addListenerForSingleValueEvent(new ValueEventListener(){
                                                        @Override
                                                        public void onDataChange(DataSnapshot dataSnapshot){
                                                            if (dataSnapshot.hasChild(receiverUserId)){
                                                                CURRENT_STATE = FRIENDS;
                                                                sendFriendRequestBtn = findViewById(R.id.profile_visit_user_friend_request);
                                                                sendFriendRequestBtn.setText(UNFRIEND);

                                                                declineFriendRequestBtn.setVisibility(View.GONE);
                                                                declineFriendRequestBtn.setEnabled(false);
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError){}
                                                    });
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError){}
                                });
                    }
                }
                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });
        }
    }

    @OnClick(R.id.profile_visit_user_friend_request)
    public void friendRequest(){
        sendFriendRequestBtn.setEnabled(false);

        if(CURRENT_STATE.equals(NOT_FRIENDS)){
            sendFriendRequest();
        }
        if (CURRENT_STATE.equals(REQUEST_SENT)){
            cancelFriendRequest();
        }
        if (CURRENT_STATE.equals(REQUEST_RECEIVED)){
            acceptFriendRequest();
        }
        if (CURRENT_STATE.equals(FRIENDS)){
            removeFriend();
        }
    }
    @OnClick(R.id.profile_visit_user_decline_request)
    public void declineRequest(){
        declineFriendRequest();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void declineFriendRequest(){
        friendRequestReference.child(senderUserId).child(receiverUserId).removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        friendRequestReference.child(receiverUserId).child(senderUserId).removeValue()
                                .addOnCompleteListener(new OnCompleteListener<Void>()
                                {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task)
                                    {
                                        if(task.isSuccessful()){
                                            sendFriendRequestBtn.setEnabled(true);
                                            CURRENT_STATE= NOT_FRIENDS;
                                            sendFriendRequestBtn.setText(SEND_FRIEND_REQUEST);
                                            declineFriendRequestBtn.setVisibility(View.GONE);
                                            declineFriendRequestBtn.setEnabled(false);
                                        }
                                    }
                                });
                    }
                });
    }

    private void removeFriend(){
        friendsReference.child(senderUserId).child(receiverUserId).removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if(task.isSuccessful()){
                    friendsReference.child(receiverUserId).child(senderUserId).removeValue()
                            .addOnCompleteListener(new OnCompleteListener<Void>()
                            {
                                @Override
                                public void onComplete(@NonNull Task<Void> task)
                                {
                                    if(task.isSuccessful()){
                                        sendFriendRequestBtn = findViewById(R.id.profile_visit_user_friend_request);
                                        sendFriendRequestBtn.setEnabled(true);
                                        CURRENT_STATE= NOT_FRIENDS;
                                        sendFriendRequestBtn.setText(SEND_FRIEND_REQUEST);

                                        declineFriendRequestBtn.setVisibility(View.GONE);
                                        declineFriendRequestBtn.setEnabled(false);
                                    }
                                }
                            });
                }
            }
        });
    }

    private void acceptFriendRequest(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("dd-MM-YYYY");
        final String saveCurrentDate = currentDate.format(cal.getTime());

        friendsReference.child(senderUserId).child(receiverUserId).child(DATE).setValue(saveCurrentDate)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid)
                    {
                        friendsReference.child(receiverUserId).child(senderUserId).child(DATE).setValue(saveCurrentDate)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid)
                                    {
                                        friendRequestReference.child(senderUserId).child(receiverUserId).removeValue()
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        friendRequestReference.child(receiverUserId).child(senderUserId).removeValue()
                                                                .addOnCompleteListener(new OnCompleteListener<Void>()
                                                                {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<Void> task)
                                                                    {
                                                                        if(task.isSuccessful())
                                                                        {
                                                                            sendFriendRequestBtn.setEnabled(true);
                                                                            CURRENT_STATE= FRIENDS;
                                                                            sendFriendRequestBtn.setText(UNFRIEND);
                                                                            declineFriendRequestBtn.setVisibility(View.GONE);
                                                                            declineFriendRequestBtn.setEnabled(false);
                                                                        }
                                                                    }
                                                                });
                                                    }
                                                });
                                    }
                                });
                    }
                });
    }

    private void cancelFriendRequest(){
        friendRequestReference.child(senderUserId).child(receiverUserId).removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        friendRequestReference.child(receiverUserId).child(senderUserId).removeValue()
                                .addOnCompleteListener(new OnCompleteListener<Void>()
                                {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task)
                                    {
                                        if(task.isSuccessful()){
                                            sendFriendRequestBtn.setEnabled(true);
                                            CURRENT_STATE= NOT_FRIENDS;
                                            sendFriendRequestBtn.setText(SEND_FRIEND_REQUEST);

                                            declineFriendRequestBtn.setVisibility(View.GONE);
                                            declineFriendRequestBtn.setEnabled(false);
                                        }
                                    }
                                });
                    }
                });
    }
    private void sendFriendRequest(){
        friendRequestReference.child(senderUserId).child(receiverUserId).child(REQUEST_TYPE).setValue(SENT)
        .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
            if (task.isSuccessful()){
                friendRequestReference.child(receiverUserId).child(senderUserId).child(REQUEST_TYPE).setValue(RECEIVED)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task)
                            {
                                if(task.isSuccessful()){
                                    HashMap <String, String> notificationsData = new HashMap <>();
                                    notificationsData.put(FROM, senderUserId);
                                    notificationsData.put(TYPE, REQUEST);
                                    notificationsReference.child(receiverUserId).push().setValue(notificationsData)
                                            .addOnCompleteListener(new OnCompleteListener<Void>()
                                            {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task)
                                                {
                                                    if(task.isSuccessful()){
                                                        sendFriendRequestBtn.setEnabled(true);
                                                        CURRENT_STATE = REQUEST_SENT;
                                                        sendFriendRequestBtn.setText(CANCEL_FRIEND_REQUEST);
                                                        declineFriendRequestBtn.setVisibility(View.GONE);
                                                        declineFriendRequestBtn.setEnabled(false);
                                                    }
                                                }
                                            });
                                }
                            }
                        });
             }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unbinder.unbind();
    }
}
