package pl.mdiner.foodbuddy.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import pl.mdiner.foodbuddy.R;

import static pl.mdiner.foodbuddy.utilities.Static.IS_LOGGED;

public class ResetPasswordActivity extends AppCompatActivity{

    @BindView(R.id.etEmail)
    EditText emailField;
    private FirebaseAuth mAuth;
    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        unbinder = ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
    }

    private void resetPassword(){

        String email = emailField.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplication(), R.string.enter_email, Toast.LENGTH_SHORT).show();
            return;
        }
        showProgressDialog();
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(ResetPasswordActivity.this,
                            R.string.email_instructions, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ResetPasswordActivity.this,
                            R.string.failed_instructions, Toast.LENGTH_SHORT).show();
                }
                hideProgressDialog();
            }
        });
    }

   @OnClick (R.id.btnReset)
   public void resetButton(){
       resetPassword();
       startSpecificActivity(WelcomeActivity.class, false);
   }
   @OnClick (R.id.tvLogin)
   public void login (){
       startSpecificActivity(WelcomeActivity.class, false);
   }

   public void startSpecificActivity(Class<?> otherActivityClass, boolean isLogged) {
        Intent intent = new Intent(this, otherActivityClass);
        intent.putExtra(IS_LOGGED, false);
        startActivity(intent);
    }

    @VisibleForTesting
    private ProgressDialog mProgressDialog;

    protected void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }


    protected void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}


