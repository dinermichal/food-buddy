package pl.mdiner.foodbuddy.ui.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import pl.mdiner.foodbuddy.R;
import pl.mdiner.foodbuddy.maps.InfoWindowCustom;
import pl.mdiner.foodbuddy.ui.fragments.chat.ChatFragmentsListActivity;
import pl.mdiner.foodbuddy.utilities.FacebookUtils;
import pl.mdiner.foodbuddy.utilities.ImageCallback;
import pl.mdiner.foodbuddy.utilities.MarkersListener;
import pl.mdiner.foodbuddy.utilities.Utils;

import static android.text.TextUtils.isEmpty;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_FIRST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_LAST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_PROFILE_PIC;
import static pl.mdiner.foodbuddy.utilities.Static.FACEBOOK_FIRST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.FACEBOOK_LAST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.FACEBOOK_PROFILE_PIC;
import static pl.mdiner.foodbuddy.utilities.Static.LOG_OUT;
import static pl.mdiner.foodbuddy.utilities.Static.MARKER_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.ONLINE;
import static pl.mdiner.foodbuddy.utilities.Static.THUMB_PHOTO_URL;
import static pl.mdiner.foodbuddy.utilities.Static.USERS;
import static pl.mdiner.foodbuddy.utilities.Static.WROCLAW;
import static pl.mdiner.foodbuddy.utilities.Static.mJadka;
import static pl.mdiner.foodbuddy.utilities.Static.mKonspira;
import static pl.mdiner.foodbuddy.utilities.Static.mPrzystan;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnInfoWindowClickListener{

    public static final String JADKA = "Restauracja Jadka";
    public static final String PRZYSTAN = "Restauracja Przystan";
    public static final String KONSPIRA = "Restauracja Konspira";
    // Markers
    private final HashMap<String, String> markerMap = new HashMap<>();
    // Maps
    private boolean mPermissionsGranted = false;
    private SupportMapFragment sMapFragment;
    private GoogleMap mMap;
    private ImageView profileImage;
    private TextView profileName;
    private TextView editProfile;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    //Firebase
    private DatabaseReference mDatabase;
    private String profileImageLink;
    private DatabaseReference userReference;
    private LoginManager mAuthFacebook;
    private String onlineUserID;
    private FirebaseUser user;
    private MarkersListener mCallback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Acquire a reference to the system Location Manager
                LocationManager locationManager =
                        (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                //Acquire the user's location
                @SuppressLint("MissingPermission")
                Location selfLocation = locationManager
                        .getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

                //Move the map to the user's location
                LatLng selfLoc = new LatLng(selfLocation.getLatitude(), selfLocation.getLongitude());
                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(selfLoc, 15);
                mMap.animateCamera(update);
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initializeMaps();

        View parentView = navigationView.getHeaderView(0);

        profileImage = parentView.findViewById(R.id.user_profile_image);
        profileName = parentView.findViewById(R.id.profile_name);
        editProfile = parentView.findViewById(R.id.edit_profile);
        ProgressBar progressBar;
        progressBar = parentView.findViewById(R.id.progressBar);

        mAuthFacebook = LoginManager.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.keepSynced(true);
        final ProgressBar finalProgressBar = progressBar;
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    final String onlineUserId = user.getUid();
                    userReference = mDatabase.child(USERS).child(onlineUserId);
                    isOnline(true);

                    finalProgressBar.setVisibility(View.VISIBLE);
                    ///Facebook
                    if (FacebookUtils.isLoggedIn()) {

                        String firstName = Utils.getPreferences(FACEBOOK_FIRST_NAME, MainActivity.this);
                        String lastName = Utils.getPreferences(FACEBOOK_LAST_NAME, MainActivity.this);
                        profileName.setText(firstName + " " + lastName);

                        mDatabase.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.child(USERS).child(onlineUserId).child(THUMB_PHOTO_URL).exists() && onlineUserId != null) {
                                    profileImageLink = dataSnapshot.child(USERS).child(onlineUserId).child(THUMB_PHOTO_URL).getValue().toString();
                                    Picasso.with(MainActivity.this).load(profileImageLink).networkPolicy(NetworkPolicy.OFFLINE)
                                                .into(profileImage, new Callback() {
                                                    @Override
                                                    public void onSuccess() {}
                                                    @Override
                                                    public void onError() {
                                                        Picasso.with(MainActivity.this).load(profileImageLink).into(profileImage,
                                                        new ImageCallback(finalProgressBar) {
                                                             @Override
                                                             public void onSuccess() {
                                                                     if (this.progressBar != null) {
                                                                     this.progressBar.setVisibility(View.GONE);
                                                        }
                                                    }
                                                });
                                                    }
                                                });
                                }
                                else{
                                    profileImageLink= Utils.getPreferences(FACEBOOK_PROFILE_PIC, MainActivity.this);
                                    if (profileImageLink.equals("")){
                                        Picasso.with(MainActivity.this).load(R.drawable.default_profile).into(profileImage);
                                    }
                                    else{
                                        Picasso.with(MainActivity.this).load(profileImageLink).networkPolicy(NetworkPolicy.OFFLINE)
                                            .into(profileImage, new Callback() {
                                                @Override
                                                public void onSuccess() {}
                                                @Override
                                                public void onError() {
                                                    Picasso.with(MainActivity.this).load(profileImageLink).into(profileImage,
                                                            new ImageCallback(finalProgressBar) {
                                                                @Override
                                                                public void onSuccess() {
                                                                    if (this.progressBar != null) {
                                                                        this.progressBar.setVisibility(View.GONE);
                                                                    }
                                                                }
                                                            });
                                                }
                                            });
                                    }
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });
                        profileName.setVisibility(View.VISIBLE);
                        editProfile.setVisibility(View.VISIBLE);
                        editProfile.setText(R.string.edit_profile);
                        editProfile.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent listIntent = new Intent(MainActivity.this, ProfileDetailsActivity.class);

                                startActivity(listIntent);
                            }
                        });

                    }
                    ///Email
                    else {
                        String firstName = Utils.getPreferences(EMAIL_FIRST_NAME, MainActivity.this);
                        String lastName = Utils.getPreferences(EMAIL_LAST_NAME, MainActivity.this);
                        profileName.setText(firstName + " " + lastName);
                        mDatabase.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot.child(USERS).child(onlineUserId).child(THUMB_PHOTO_URL).exists() && user != null) {
                                    profileImageLink = dataSnapshot.child(USERS).child(onlineUserId).child(THUMB_PHOTO_URL).getValue().toString();
                                    Picasso.with(MainActivity.this).load(profileImageLink).networkPolicy(NetworkPolicy.OFFLINE)
                                            .into(profileImage, new Callback() {
                                                @Override
                                                public void onSuccess() {}
                                                @Override
                                                public void onError() {
                                                    Picasso.with(MainActivity.this).load(profileImageLink).into(profileImage,
                                                            new ImageCallback(finalProgressBar) {
                                                                @Override
                                                                public void onSuccess() {
                                                                    if (this.progressBar != null) {
                                                                        this.progressBar.setVisibility(View.GONE);
                                                                    }
                                                                }
                                                            });
                                                }
                                            });
                                } else {
                                    profileImageLink = Utils.getPreferences(EMAIL_PROFILE_PIC, MainActivity.this);
                                    if (isEmpty(profileImageLink)) {
                                        Picasso.with(MainActivity.this).load(R.drawable.default_profile).into(profileImage);
                                    }
                                    else {
                                        Picasso.with(MainActivity.this).load(profileImageLink).networkPolicy(NetworkPolicy.OFFLINE)
                                                .into(profileImage, new Callback() {
                                                    @Override
                                                    public void onSuccess() {}

                                                    @Override
                                                    public void onError() {
                                                        Picasso.with(MainActivity.this).load(profileImageLink).into(profileImage,
                                                                new ImageCallback(finalProgressBar) {
                                                                    @Override
                                                                    public void onSuccess() {
                                                                        if (this.progressBar != null) {
                                                                            this.progressBar.setVisibility(View.GONE);
                                                                        }
                                                                    }
                                                                });
                                                    }
                                                });
                                    }
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError){}
                        });
                        editProfile.setText(R.string.edit_profile);
                        editProfile.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent listIntent = new Intent(MainActivity.this, ProfileDetailsActivity.class);
                                startActivity(listIntent);
                            }
                        });
                    }
                }
                //FirebaseUser signed out
                else {
                    Intent logInIntent = new Intent(MainActivity.this, WelcomeActivity.class);
                    startActivity(logInIntent);
                }
            }
        };
    }

    @Override
    protected void onStart(){
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop(){
        super.onStop();
        if (mAuthListener != null){
            user = mAuth.getCurrentUser();
            if (user != null){
                isOnline(false);
            }
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    public void isOnline(boolean isOnline){
        if (isOnline) {
            userReference.child(ONLINE).setValue(true);
        }
        else {
            userReference.child(ONLINE).setValue(ServerValue.TIMESTAMP);
        }
    }
    @Override
    public void onBackPressed(){
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if (id == R.id.action_logout){
         logOutUser();
        }

        return super.onOptionsItemSelected(item);
    }

    private void logOutUser(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null){
            isOnline(false);
            Intent logOutIntent = new Intent(MainActivity.this, WelcomeActivity.class);
            logOutIntent.putExtra(LOG_OUT, true);
            startActivity(logOutIntent);
            finish();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item){
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.edit_profile){
        }
        if (id == R.id.chat){
            Intent listIntent = new Intent(MainActivity.this, ChatFragmentsListActivity.class);

            startActivity(listIntent);
            // Handle the camera action
        }

         else if (id == R.id.people){
            Intent listIntent = new Intent(MainActivity.this, AllUsersActivity.class);
            startActivity(listIntent);
        }
        else if (id == R.id.authors){
            Intent listIntent = new Intent(MainActivity.this, AuthorsActivity.class);
            startActivity(listIntent);
        }
        else if (id == R.id.logout){
            logOutUser();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    ////////   MAPS
    private void initializeMaps(){
        sMapFragment = SupportMapFragment.newInstance();
        sMapFragment.getMapAsync(this);
        android.support.v4.app.FragmentManager sFm = getSupportFragmentManager();

        if (!sMapFragment.isAdded())
            sFm.beginTransaction().add(R.id.map, sMapFragment).commit();
        else
            sFm.beginTransaction().show(sMapFragment).commit();
    }
    @Override
    public void onMapReady(GoogleMap googleMap){

        mMap = googleMap;

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(WROCLAW));

        addMarkersToMap();

        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setInfoWindowAdapter(new InfoWindowCustom(markerMap, this));
        enableMyLocation();

        mMap.getUiSettings().setMyLocationButtonEnabled(false);

    }
    @SuppressLint("MissingPermission")
    private void enableMyLocation(){
        if (!checkPermissions()){
            requestPermissions();
        }
        else{
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
            }
        }
    }
    private void addMarkersToMap(){
        String id;
        Marker jadka = mMap.addMarker(new MarkerOptions()
                .position(mJadka)
                .title(getString(R.string.jadka))
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMarkerBitmap())));

        id = jadka.getId();
        markerMap.put(id, JADKA);

        Marker przystan = mMap.addMarker(new MarkerOptions()
                .position(mPrzystan)
                .title(getString(R.string.przystan))
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMarkerBitmap())));

        id = przystan.getId();
        markerMap.put(id, PRZYSTAN);

        Marker konspira = mMap.addMarker(new MarkerOptions()
                .position(mKonspira)
                .title(getString(R.string.konspira))
                .icon(BitmapDescriptorFactory.fromBitmap(resizeMarkerBitmap())));

        id = konspira.getId();
        markerMap.put(id, KONSPIRA);
    }
    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location){
    }

    @Override
    public void onInfoWindowClick(Marker marker){
        String m = markerMap.get(marker.getId());
        Intent listIntent = new Intent(MainActivity.this, WelcomeActivity.class);
        listIntent.putExtra(MARKER_NAME, m);
        startActivity(listIntent);
    }


    //// MISC

    private Bitmap resizeMarkerBitmap(){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),getResources().getIdentifier("marker", "drawable", getPackageName()));
        return Bitmap.createScaledBitmap(imageBitmap, 120, 196, false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case 1:
                mPermissionsGranted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    private boolean checkPermissions() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
    }
    @Override
    protected void onResumeFragments(){
        super.onResumeFragments();
        if (!mPermissionsGranted){
            showMissingPermissionError();
        }
    }

    private void showMissingPermissionError(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Permissions missing").setTitle("Error").setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
            }
        });
    }

}

