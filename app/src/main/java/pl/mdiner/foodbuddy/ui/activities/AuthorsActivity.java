package pl.mdiner.foodbuddy.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import pl.mdiner.foodbuddy.R;
import pl.mdiner.foodbuddy.json.JSONauthors;

/**
 * Created by Michał Diner on 25.07.2018.
 *
 * Example class with Async usage to fill listview.
 */
public class AuthorsActivity extends AppCompatActivity {

    @BindView(R.id.lvAuthors)
    ListView listView;
    private Unbinder unbinder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authors);
        unbinder = ButterKnife.bind(this);
        new JSONauthors(this, listView).execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
