package pl.mdiner.foodbuddy.ui.fragments.chat;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.mdiner.foodbuddy.R;
import pl.mdiner.foodbuddy.chat.Chats;
import pl.mdiner.foodbuddy.ui.activities.ChatActivity;

import static pl.mdiner.foodbuddy.utilities.Static.AGE;
import static pl.mdiner.foodbuddy.utilities.Static.FIRST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.FRIENDS;
import static pl.mdiner.foodbuddy.utilities.Static.FULL_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.LAST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.ONLINE;
import static pl.mdiner.foodbuddy.utilities.Static.ONLINE_USER_ID;
import static pl.mdiner.foodbuddy.utilities.Static.THUMB_PHOTO_URL;
import static pl.mdiner.foodbuddy.utilities.Static.TRUE;
import static pl.mdiner.foodbuddy.utilities.Static.USERS;
import static pl.mdiner.foodbuddy.utilities.Static.USER_STATUS;
import static pl.mdiner.foodbuddy.utilities.Static.VISIT_USER_ID;


public class ChatsFragment extends Fragment {


    private FriendsFragment.OnFragmentInteractionListener mListener;
    private RecyclerView myChatsList;
    private DatabaseReference friendsReference;
    private String onlineUserId;
    private DatabaseReference userReference;

    public ChatsFragment() {
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_chats, container, false);

        myChatsList = view.findViewById(R.id.chats_list);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        onlineUserId = mAuth.getCurrentUser().getUid();
        friendsReference = FirebaseDatabase.getInstance().getReference().child(FRIENDS).child(onlineUserId);
        userReference = FirebaseDatabase.getInstance().getReference().child(USERS);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager((getContext()));
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);

        myChatsList.setLayoutManager(linearLayoutManager);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        userReference.child(onlineUserId).child(ONLINE).setValue(true);
        FirebaseRecyclerOptions<Chats> options =
                new FirebaseRecyclerOptions.Builder<Chats>()
                        .setQuery(friendsReference, Chats.class)
                        .build();

        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<Chats, ChatsFragment.ChatsViewHolder> (options){
            @NonNull
            @Override
            public ChatsFragment.ChatsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.all_users_display_layout, parent, false);

                return new ChatsFragment.ChatsViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull final ChatsFragment.ChatsViewHolder viewHolder, int position, @NonNull Chats model){

                final String listUserId = getRef(position).getKey();

                userReference.child(listUserId).addValueEventListener(new ValueEventListener(){
                    @Override
                    public void onDataChange(final DataSnapshot dataSnapshot){
                        String userStatus = dataSnapshot.child(USER_STATUS).getValue().toString();
                        final String fName =dataSnapshot.child(FIRST_NAME).getValue().toString();
                        final String lName =dataSnapshot.child(LAST_NAME).getValue().toString();
                        String thumbImage =dataSnapshot.child(THUMB_PHOTO_URL).getValue().toString();
                        String age =dataSnapshot.child(AGE).getValue().toString();
                        if(dataSnapshot.hasChild(ONLINE)){
                            String onlineStatus = dataSnapshot.child(ONLINE).getValue().toString();
                            viewHolder.setFriendOnline(onlineStatus);
                        }

                        viewHolder.setFriendAge(age);
                        viewHolder.setFriendName(fName + " " + lName);
                        viewHolder.setFriendThumbImage(thumbImage, getContext());
                        viewHolder.setFriendStatus(userStatus);
                        viewHolder.mView.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                if(dataSnapshot.child(ONLINE).exists()){
                                    Intent chatIntent = new Intent(getContext(), ChatActivity.class);
                                    chatIntent.putExtra(VISIT_USER_ID, listUserId);
                                    chatIntent.putExtra(FULL_NAME, fName + " " + lName);
                                    chatIntent.putExtra(ONLINE_USER_ID, onlineUserId);
                                    startActivity(chatIntent);
                                }
                                else{
                                    userReference.child(listUserId).child(ONLINE)
                                            .setValue(ServerValue.TIMESTAMP).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid)
                                        {
                                            Intent chatIntent = new Intent(getContext(), ChatActivity.class);
                                            chatIntent.putExtra(VISIT_USER_ID, listUserId);
                                            chatIntent.putExtra(FULL_NAME, fName + " " + lName);
                                            chatIntent.putExtra(ONLINE_USER_ID, onlineUserId);
                                            startActivity(chatIntent);
                                        }
                                    });
                                }
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        };
        myChatsList.setAdapter(adapter);
        adapter.startListening();
    }

    private static class ChatsViewHolder extends RecyclerView.ViewHolder{
        final View mView;

        ChatsViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        void setFriendAge(String friends_age)
        {
            TextView ageView = mView.findViewById(R.id.all_users_age);
            ageView.setText(friends_age);
        }
        void setFriendName(String friend_name)
        {
            TextView name = mView.findViewById(R.id.all_users_username);
            name.setText(friend_name);
        }
        void setFriendThumbImage(final String friendThumbImage, final Context context)
        {
            final CircleImageView image = mView.findViewById(R.id.all_users_profile_image);
            //
            Picasso.with(context).load(friendThumbImage).networkPolicy(NetworkPolicy.OFFLINE).into(image, new Callback() {
                @Override
                public void onSuccess() {}

                @Override
                public void onError(){
                    Picasso.with(context).load(friendThumbImage).into(image);
                }
            });
        }

        void setFriendOnline(String onlineStatus){
            ImageView onlineStatusView = mView.findViewById(R.id.all_users_online_status);
            if (onlineStatus.equals(TRUE)){
                onlineStatusView.setVisibility(View.VISIBLE);
            }
        }

        public void setFriendStatus(String userStatus) {
            TextView userStatusTV = mView.findViewById(R.id.all_users_description);
            userStatusTV.setText(userStatus);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FriendsFragment.OnFragmentInteractionListener) {
            mListener = (FriendsFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction();
    }
}

