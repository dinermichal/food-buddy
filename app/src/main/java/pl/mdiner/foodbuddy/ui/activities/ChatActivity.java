package pl.mdiner.foodbuddy.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import pl.mdiner.foodbuddy.R;
import pl.mdiner.foodbuddy.adapters.MessagesAdapter;
import pl.mdiner.foodbuddy.chat.Messages;

import static pl.mdiner.foodbuddy.utilities.Static.ACTIVE_AN_HOUR_AGO;
import static pl.mdiner.foodbuddy.utilities.Static.ACTIVE_A_MINUTE_AGO;
import static pl.mdiner.foodbuddy.utilities.Static.ACTIVE_FEW_SECONDS_AGO;
import static pl.mdiner.foodbuddy.utilities.Static.ACTIVE_YESTERDAY;
import static pl.mdiner.foodbuddy.utilities.Static.DAYS_AGO_HAVE_BEEN_ACTIVE;
import static pl.mdiner.foodbuddy.utilities.Static.DAY_MILLIS;
import static pl.mdiner.foodbuddy.utilities.Static.FROM;
import static pl.mdiner.foodbuddy.utilities.Static.FULL_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.HOUR;
import static pl.mdiner.foodbuddy.utilities.Static.HOURS_AGO;
import static pl.mdiner.foodbuddy.utilities.Static.IMAGE;
import static pl.mdiner.foodbuddy.utilities.Static.JPG;
import static pl.mdiner.foodbuddy.utilities.Static.MESSAGE;
import static pl.mdiner.foodbuddy.utilities.Static.MESSAGES;
import static pl.mdiner.foodbuddy.utilities.Static.MESSAGES_PICTURES;
import static pl.mdiner.foodbuddy.utilities.Static.MINUTE;
import static pl.mdiner.foodbuddy.utilities.Static.MINUTES_AGO;
import static pl.mdiner.foodbuddy.utilities.Static.ONLINE;
import static pl.mdiner.foodbuddy.utilities.Static.PICK_IMAGE_REQUEST;
import static pl.mdiner.foodbuddy.utilities.Static.SEEN;
import static pl.mdiner.foodbuddy.utilities.Static.TEXT;
import static pl.mdiner.foodbuddy.utilities.Static.THUMB_PHOTO_URL;
import static pl.mdiner.foodbuddy.utilities.Static.TIME;
import static pl.mdiner.foodbuddy.utilities.Static.TRUE;
import static pl.mdiner.foodbuddy.utilities.Static.TYPE;
import static pl.mdiner.foodbuddy.utilities.Static.USERS;
import static pl.mdiner.foodbuddy.utilities.Static.VISIT_USER_ID;
import static pl.mdiner.foodbuddy.utilities.Utils.resize;


public class ChatActivity extends AppCompatActivity{

    private String messageSenderId;
    private String messageReceiverId;
    private String messageReceiverName;
    private DatabaseReference rootRef;
    private final List<Messages> messagesList = new ArrayList<>();
    private MessagesAdapter messagesAdapter;
    private StorageReference messageImageStorageRef;
    private ProgressDialog loadingBar;
    private Bitmap thumb_bitmap;
    private Unbinder unbinder;

    @BindView(R.id.custom_chat_username)
    TextView userNameTitle;
    @BindView(R.id.custom_user_last_seen)
    TextView userLastSeen;
    @BindView(R.id.custom_user_prof_image)
    CircleImageView userChatProfileImage;
    @BindView(R.id.messageEt)
    EditText inputMessageText;
    @BindView(R.id.send_button)
    ImageButton sendMessageButton;
    @BindView(R.id.add_photo)
    ImageButton selectImageButton;
    @BindView(R.id.recycler_view_messages)
    RecyclerView userMessageList;
    private String downloadUrl;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_window);
        unbinder = ButterKnife.bind(this);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        messageSenderId= mAuth.getCurrentUser().getUid();
        messageReceiverId = getIntent().getExtras().get(VISIT_USER_ID).toString();
        messageReceiverName = getIntent().getExtras().get(FULL_NAME).toString();
        messageImageStorageRef = FirebaseStorage.getInstance().getReference().child(MESSAGES_PICTURES);
        Toolbar chatToolBar = findViewById(R.id.chat_custom_bar);
        setSupportActionBar(chatToolBar);
        LayoutInflater layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewGroup nullParent = null;
        View actionBarView  = layoutInflater.inflate(R.layout.chat_custom_bar, nullParent);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setCustomView(actionBarView);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        loadingBar = new ProgressDialog(this);


        messagesAdapter = new MessagesAdapter(messagesList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        userMessageList.setHasFixedSize(true);
        userMessageList.setLayoutManager(linearLayoutManager);
        userMessageList.setAdapter(messagesAdapter);
        fetchMessages();
        userNameTitle.setText(messageReceiverName);
        rootRef = FirebaseDatabase.getInstance().getReference();

        rootRef.child(USERS).child(messageReceiverId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String online = dataSnapshot.child(ONLINE).getValue().toString();
                final String userThumb = dataSnapshot.child(THUMB_PHOTO_URL).getValue().toString();
                Picasso.with(ChatActivity.this).load(userThumb).networkPolicy(NetworkPolicy.OFFLINE).into(userChatProfileImage, new Callback() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError(){
                        Picasso.with(ChatActivity.this).load(userThumb).into(userChatProfileImage);
                    }
                });
                if (online.equals(TRUE)){
                    userLastSeen.setText(ONLINE);
                }
                else{
                    long lastSeen = Long.parseLong(online);
                    String lastSeenDisplayTime = whenUserWasLastSeen(lastSeen);
                    userLastSeen.setText(lastSeenDisplayTime);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }
    public static String whenUserWasLastSeen(long time) {
        if (time < 1000000000000L) time *= 1000;
        long now = System.currentTimeMillis();
        if (time > now || time <= 0) return null;
        final long diff = now - time;
        if (diff < MINUTE) return ACTIVE_FEW_SECONDS_AGO;
        else if (diff < 2 * MINUTE) return ACTIVE_A_MINUTE_AGO;
        else if (diff < 50 * MINUTE) return diff / MINUTE + MINUTES_AGO;
        else if (diff < 90 * MINUTE) return ACTIVE_AN_HOUR_AGO;
        else if (diff < 24 * HOUR) return diff / HOUR + HOURS_AGO;
        else if (diff < 48 * HOUR) return ACTIVE_YESTERDAY;
        else return diff / DAY_MILLIS + DAYS_AGO_HAVE_BEEN_ACTIVE;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        {super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                    && data != null && data.getData() != null) {
                loadingBar.setTitle(getString(R.string.sending_chat_image));
                loadingBar.setMessage(getString(R.string.please_wait));
                loadingBar.show();
                Uri filePath = data.getData();
                final String messageSenderRef = MESSAGES + "/" + messageSenderId + "/" + messageReceiverId;
                final String messageReceiverRef = MESSAGES + "/" + messageReceiverId + "/" + messageSenderId;
                DatabaseReference userMessageKey = rootRef.child(MESSAGES).child(messageSenderId)
                        .child(messageReceiverId).push();
                final String messagePushId = userMessageKey.getKey();
                final StorageReference filePathStorage = messageImageStorageRef.child(messagePushId + JPG);
                try {
                    thumb_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), filePath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                resize(thumb_bitmap, 1000, 1000).compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
                final byte[] thumb_byte = byteArrayOutputStream.toByteArray();

                UploadTask uploadTask = filePathStorage.putBytes(thumb_byte);
                uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> thumbTask) {
                        if (thumbTask.isSuccessful()) {
                            filePathStorage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    downloadUrl = String.valueOf(uri);
                                    Map<String, Object> messageTextBody = new HashMap<>();
                                    messageTextBody.put(MESSAGE, downloadUrl);
                                    messageTextBody.put(SEEN, false);
                                    messageTextBody.put(TYPE, IMAGE);
                                    messageTextBody.put(TIME, ServerValue.TIMESTAMP);
                                    messageTextBody.put(FROM, messageSenderId);

                                    Map<String, Object> messageBodyDetails = new HashMap<>();
                                    messageBodyDetails.put(messageSenderRef + "/" + messagePushId, messageTextBody);
                                    messageBodyDetails.put(messageReceiverRef + "/" + messagePushId, messageTextBody);
                                    rootRef.updateChildren(messageBodyDetails, new DatabaseReference.CompletionListener() {
                                        @Override
                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                            inputMessageText.setText("");
                                            loadingBar.dismiss();
                                        }
                                    });
                                    loadingBar.dismiss();
                                }
                            });
                        }
                    }
                });
            }
        }
    }

    private void fetchMessages(){
        FirebaseDatabase.getInstance().getReference().child(MESSAGES).child(messageSenderId).child(messageReceiverId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    Messages messages = dataSnapshot.getValue(Messages.class);
                    messagesList.add(messages);
                    messagesAdapter.notifyDataSetChanged();
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }


    @OnClick(R.id.send_button)
    public void send(){
        sendMessage();
    }
    @OnClick(R.id.add_photo)
    public void addPhoto(){selectImage();}

    private void selectImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), PICK_IMAGE_REQUEST);
    }

    private void sendMessage(){
        String messageText = inputMessageText.getText().toString();
        if (TextUtils.isEmpty(messageText)){
            sendMessageButton.setEnabled(false);
        }
        else{
            String messageSenderRef = MESSAGES + "/" +messageSenderId + "/" + messageReceiverId;
            String messageReceiverRef = MESSAGES +"/" + messageReceiverId + "/" + messageSenderId;
            DatabaseReference userMessageKey =  rootRef.child(MESSAGES).child(messageSenderId)
                    .child(messageReceiverId).push();
            String messagePushId=  userMessageKey.getKey();

            Map<String, Object> messageTextBody = new HashMap<>();
            messageTextBody.put(MESSAGE, messageText);
            messageTextBody.put(SEEN, false);
            messageTextBody.put(TYPE, TEXT);
            messageTextBody.put(TIME, ServerValue.TIMESTAMP);
            messageTextBody.put(FROM, messageSenderId);

            Map<String, Object> messageBodyDetails = new HashMap<>();
            messageBodyDetails.put(messageSenderRef + "/"+ messagePushId, messageTextBody);
            messageBodyDetails.put(messageReceiverRef + "/" + messagePushId, messageTextBody);

            rootRef.updateChildren(messageBodyDetails, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference)
                {
                    if (databaseError != null){
                        Log.d("Chat Log", databaseError.getMessage());
                    }
                    inputMessageText.setText("");
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
