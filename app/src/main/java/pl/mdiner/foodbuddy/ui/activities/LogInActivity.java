package pl.mdiner.foodbuddy.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import pl.mdiner.foodbuddy.R;
import pl.mdiner.foodbuddy.utilities.FacebookUtils;

import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_DEVICE_TOKEN;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_FIRST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_LAST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.EMAIL_PROFILE_PIC;
import static pl.mdiner.foodbuddy.utilities.Static.FIRST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.LAST_NAME;
import static pl.mdiner.foodbuddy.utilities.Static.PHOTO;
import static pl.mdiner.foodbuddy.utilities.Static.USERS;
import static pl.mdiner.foodbuddy.utilities.Utils.setPreferences;

public class LogInActivity extends FacebookUtils {

    private static final String TAG = "EmailPassword";

    private Unbinder unbinder;
    @BindView(R.id.tvStatus)
    TextView mStatusTextView;
    @BindView(R.id.etEmail)
    EditText mEmailField;
    @BindView(R.id.etPassword)
    EditText mPasswordField;

    private static final String LOG_TAG = LogInActivity.class.getSimpleName();


    // Facebook
    private CallbackManager mCallbackManager;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference getNameFromDatabaseReference;
    private DatabaseReference mDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);
        unbinder = ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
        LoginManager mAuthFacebook = LoginManager.getInstance();
        DatabaseReference userReference = FirebaseDatabase.getInstance().getReference().child(USERS);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    Intent intent = new Intent(LogInActivity.this, MainActivity.class);
                    startActivity(intent);

                }
            }
        };
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        // Initialize Facebook Login button
        initializeFacebookLogin(LogInActivity.this);
    }


    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser, LogInActivity.this);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }

        showProgressDialog();
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>()
                {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task)
                    {
                        if(task.isSuccessful()){
                            downloadUserDataFromDatabaseToPreferences(mAuth.getCurrentUser().getUid());
                            String deviceToken = FirebaseInstanceId.getInstance().getToken();
                            setPreferences(EMAIL_DEVICE_TOKEN, deviceToken, LogInActivity.this);
                        }
                        else{
                            try {
                                throw task.getException();
                            } catch (FirebaseAuthInvalidUserException e) {
                                mStatusTextView.setError("Invalid Emaild Id");
                                mStatusTextView.requestFocus();
                            } catch (FirebaseAuthInvalidCredentialsException e) {
                              //  Log.d(LOG_TAG , "email :" + email);
                                mStatusTextView.setError("Invalid Password");
                                mStatusTextView.requestFocus();
                            } catch (FirebaseNetworkException e) {
                                Toast.makeText(LogInActivity.this,"error_message_failed_sign_in_no_network",
                                        Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                Log.e(LOG_TAG, e.getMessage());
                            }
                            Log.w(LOG_TAG, "signInWithEmail:failed", task.getException());
                            Toast.makeText(LogInActivity.this, R.string.auth_failed,
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null, LogInActivity.this);
                        }
                }});
    }

    private void downloadUserDataFromDatabaseToPreferences(final String id){
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    getNameFromDatabaseReference = FirebaseDatabase.getInstance().getReference().child(USERS).child(id);
                    getNameFromDatabaseReference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String firstName = dataSnapshot.child(FIRST_NAME).getValue().toString();
                            String lastName = dataSnapshot.child(LAST_NAME).getValue().toString();
                            String photoUrl = dataSnapshot.child(PHOTO).getValue().toString();
                            setPreferences(EMAIL_FIRST_NAME, firstName, LogInActivity.this);
                            setPreferences(EMAIL_LAST_NAME, lastName, LogInActivity.this);
                            setPreferences(EMAIL_PROFILE_PIC, photoUrl, LogInActivity.this);
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    });
                }
            }
        };
    }

    @OnClick(R.id.btnEmailCreateAccount)
    public void createAccount(){
        startSpecificActivity(CreateUserActivity.class);
    }

    public void startSpecificActivity(Class<?> otherActivityClass) {
        Intent intent = new Intent(this, otherActivityClass);
        startActivity(intent);
    }
    @OnClick(R.id.btnEmailSignIn)
    public void signIn(){
        signIn(mEmailField.getText().toString().trim().toLowerCase(), mPasswordField.getText().toString().trim());
    }

    @OnClick(R.id.tvForgotPassword)
    public void forgotPassword(){
        Intent listIntent = new Intent(this, ResetPasswordActivity.class);
        startActivity(listIntent);
    }

    private boolean validateForm() {
        boolean valid = true;
        String email = mEmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEmailField.setError("Required.");
            mEmailField.requestFocus();
            valid = false;
        } else {
            mEmailField.setError(null);
        }
        String password = mPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            mPasswordField.setError("Required.");
            mPasswordField.requestFocus();
            valid = false;
        } else {
            mPasswordField.setError(null);
        }
        return valid;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}

