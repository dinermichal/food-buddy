package pl.mdiner.foodbuddy.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.mdiner.foodbuddy.R;
import pl.mdiner.foodbuddy.firebase.FirebaseUser;

import static pl.mdiner.foodbuddy.utilities.Static.USERS;
import static pl.mdiner.foodbuddy.utilities.Static.VISIT_USER_ID;


public class AllUsersActivity extends AppCompatActivity {

    private RecyclerView allUsersList;
    private DatabaseReference allDatabaseUsersReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_users);
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        allUsersList = findViewById(R.id.all_users_list);
        allUsersList.setHasFixedSize(true);
        allUsersList.setLayoutManager(new LinearLayoutManager(this));
        allDatabaseUsersReference = FirebaseDatabase.getInstance().getReference().child(USERS);
    }

    @Override
    protected void onStart(){
        super.onStart();

        FirebaseRecyclerOptions<FirebaseUser> options =
                new FirebaseRecyclerOptions.Builder<FirebaseUser>()
                        .setQuery(allDatabaseUsersReference, FirebaseUser.class)
                        .build();

        FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<FirebaseUser, AllUsersViewHolder> (options)
            {
            @NonNull
            @Override
            public AllUsersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.all_users_display_layout, parent, false);

                return new AllUsersViewHolder (view);
            }

            @Override
            protected void onBindViewHolder(@NonNull final AllUsersViewHolder viewHolder, int position, @NonNull FirebaseUser model)
            {
                viewHolder.setUserName(model.getFirstName()+ " " + model.getLastName());
                viewHolder.setUserDescription(model.getDescription());
                viewHolder.setUserThumbImage(getApplicationContext(), model.getThumbPhotoUrl());
                viewHolder.setUserAge("Age: " + model.getAge());

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        String visitUserId = getRef(viewHolder.getAdapterPosition()).getKey();
                        Intent userDetailsIntent = new Intent (AllUsersActivity.this, AllUsersDetailsActivity.class);
                        userDetailsIntent.putExtra(VISIT_USER_ID, visitUserId);
                        startActivity(userDetailsIntent);
                    }
                });
            }
        };
        allUsersList.setAdapter(adapter);
        adapter.startListening();
    }

    private static class AllUsersViewHolder extends RecyclerView.ViewHolder{

        private final View mView;
        private TextView name;
        private TextView description;

        private TextView age;

        AllUsersViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }
        void setUserName(String userName)
        {
            name = mView.findViewById(R.id.all_users_username);
            name.setText(userName);
        }
        void setUserDescription(String userDescription)
        {
            description = mView.findViewById(R.id.all_users_description);
            description.setText(userDescription);
        }

        void setUserThumbImage(final Context context, final String userThumbImage)
        {
            final CircleImageView image = mView.findViewById(R.id.all_users_profile_image);
            Picasso.with(context).load(userThumbImage).networkPolicy(NetworkPolicy.OFFLINE).into(image, new Callback() {
                @Override
                public void onSuccess() {}
                @Override
                public void onError()
                {
                    Picasso.with(context).load(userThumbImage).into(image);
                }
            });
        }
        void setUserAge(String userAge){
            age = mView.findViewById(R.id.all_users_age);
            age.setText(userAge);
        }
    }
}
