package pl.mdiner.foodbuddy.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import pl.mdiner.foodbuddy.R;

import static pl.mdiner.foodbuddy.utilities.Static.FIRST_PAGE;
import static pl.mdiner.foodbuddy.utilities.Static.SECOND_PAGE;
import static pl.mdiner.foodbuddy.utilities.Static.THIRD_PAGE;

public class WelcomeImageViewPagerFragment extends Fragment {


    public static final String PAGE_NUMBER = "PAGE_NUMBER";
    private int pageNumber;
    private Unbinder unbinder;

    @BindView(R.id.pager_image)
    ImageView mPagerImage;

    public static WelcomeImageViewPagerFragment init(int position){
        WelcomeImageViewPagerFragment fragment = new WelcomeImageViewPagerFragment();
        Bundle args = new Bundle();
        args.putInt(PAGE_NUMBER, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageNumber = getArguments() != null ? getArguments().getInt(PAGE_NUMBER) : 1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.viewpager_image,container,false);
        unbinder = ButterKnife.bind(this, view);
        //Sets image depending on page number.
        whichImage(pageNumber, mPagerImage, getContext());
        return view;
    }

    @SuppressLint("NewApi")
    public void whichImage (int position, ImageView view, Context context){
        switch (position) {
            case FIRST_PAGE:
                view.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.restauracja_jadka));
                break;
            case SECOND_PAGE:
                view.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.restauracja_konspira));
                break;
            case THIRD_PAGE:
                view.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.restauracja_przystan));
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
