package pl.mdiner.foodbuddy.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import pl.mdiner.foodbuddy.R;
import pl.mdiner.foodbuddy.utilities.Author;


public class AuthorsAdapter extends BaseAdapter{

    private Context context;
    ArrayList<Author> authors;

    public AuthorsAdapter(Context c, ArrayList<Author> authors) {
        this.context = c;
        this.authors = authors;
    }

    @Override
    public int getCount() {
        return authors.size();
    }

    @Override
    public Object getItem(int i) {
        return authors.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if(view==null){
            view=LayoutInflater.from(context).inflate(R.layout.authors_card,viewGroup,false);
        }
        TextView firstNameTxt= view.findViewById(R.id.firstNameTxt);
        TextView lastNameTxt= view.findViewById(R.id.lastNameTxt);
        TextView emailTxt= view.findViewById(R.id.emailTxt);
        TextView phoneTxt= view.findViewById(R.id.phoneTxt);
        ImageView photoIv = view.findViewById(R.id.authorPhotoIv);

        Author author= (Author) this.getItem(i);

        final String firstName=author.getFirstName();
        final String lastName=author.getLastName();
        final String email=author.getEmail();
        final String phone =author.getPhone();
        final String photo =author.getPhoto();

        firstNameTxt.setText(firstName);
        lastNameTxt.setText(lastName);
        emailTxt.setText(email);
        phoneTxt.setText(phone);
        Picasso.with(context).load(photo).into(photoIv);

        return view;
    }
}













