package pl.mdiner.foodbuddy.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import pl.mdiner.foodbuddy.ui.fragments.WelcomeImageViewPagerFragment;

import static pl.mdiner.foodbuddy.utilities.Static.FIRST_PAGE;
import static pl.mdiner.foodbuddy.utilities.Static.SECOND_PAGE;
import static pl.mdiner.foodbuddy.utilities.Static.THIRD_PAGE;

public class WelcomeImageAdapter extends FragmentPagerAdapter {

    public WelcomeImageAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Fragment getItem(int page) {
        switch (page) {
            case FIRST_PAGE:
                return WelcomeImageViewPagerFragment.init(page);
            case SECOND_PAGE:
                return WelcomeImageViewPagerFragment.init(page);
            case THIRD_PAGE:
                return WelcomeImageViewPagerFragment.init(page);
        }
        return null;
    }
}