package pl.mdiner.foodbuddy.json;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import pl.mdiner.foodbuddy.adapters.AuthorsAdapter;
import pl.mdiner.foodbuddy.ui.activities.AuthorsActivity;
import pl.mdiner.foodbuddy.utilities.Author;

import static pl.mdiner.foodbuddy.utilities.Static.JSON_URL;
import static pl.mdiner.foodbuddy.utilities.Utils.download;

public class JSONauthors extends AsyncTask<Void,Void,String> {

    private final WeakReference<Context> appReference;
    private final WeakReference<ListView> authorsList;
    private ArrayList<Author> authors =new ArrayList<Author>();

    public JSONauthors(Context context, ListView listView) {
        appReference = new WeakReference<>(context);
        authorsList = new WeakReference<>(listView);
    }
    @Override
    protected String doInBackground(Void... voids) {

        Uri builtUri = Uri.parse(JSON_URL);

        String response;
        try {
            response  = download(builtUri);
            return response;
        }catch (Exception e){
            return null;
        }
    }

    @Override
    protected void onPostExecute(String response) {
        if (response != null) {
            if (parse(response)){
                AuthorsActivity activity = (AuthorsActivity) appReference.get();
                ListView list = authorsList.get();
                list.setAdapter(new AuthorsAdapter(activity, authors));
            };
        }

    }

    private boolean parse(String jsonString){
        boolean result = false;
        try{
            authors.clear();
            Author author;
            JSONObject baseJsonResponse = new JSONObject(jsonString);
            JSONArray peopleArray = baseJsonResponse.getJSONArray("results");

            for (int i=0;i<peopleArray.length();i++)
            {
                JSONObject currentPerson = peopleArray.getJSONObject(i);
                JSONObject detailedNames = currentPerson.getJSONObject("name");
                JSONObject picture = currentPerson.getJSONObject("picture");
                String firstName =detailedNames.getString("first");
                String lastName =detailedNames.getString("last");
                String email = currentPerson.getString("email");
                String phone =currentPerson.getString("phone");
                String photo =picture.getString("large");

                author = new Author();

                author.setFirstName(firstName);
                author.setLastName(lastName);
                author.setEmail(email);
                author.setPhone(phone);
                author.setPhoto(photo);
                authors.add(author);
                result = true;
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
}
