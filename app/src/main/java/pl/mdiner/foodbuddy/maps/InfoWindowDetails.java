package pl.mdiner.foodbuddy.maps;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import pl.mdiner.foodbuddy.R;
import pl.mdiner.foodbuddy.utilities.MarkersListener;
import pl.mdiner.foodbuddy.utilities.Utils;

import static pl.mdiner.foodbuddy.ui.activities.MainActivity.JADKA;
import static pl.mdiner.foodbuddy.ui.activities.MainActivity.KONSPIRA;
import static pl.mdiner.foodbuddy.ui.activities.MainActivity.PRZYSTAN;
import static pl.mdiner.foodbuddy.utilities.Static.MARKER_DETAILS;
import static pl.mdiner.foodbuddy.utilities.Static.MARKER_NAME;


public class InfoWindowDetails extends Fragment {
    private Unbinder unbinder;

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.snippet)
    TextView snippet;
    @BindView(R.id.badge)
    ImageView badge;
    private MarkersListener mCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (MarkersListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement Listener");
        }
    }

    public InfoWindowDetails() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.custom_info_window, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        String markerName=getArguments().getString(MARKER_NAME);

        if (markerName!= null){
            if (markerName.equals(JADKA)) {
                snippet.setText(R.string.lorem_ipsum_detail);
                title.setText(markerName);
                badge.setImageResource(R.drawable.restauracja_jadka);
                Utils.setPreferences(MARKER_NAME, markerName, getContext());
                Utils.setPreferences(MARKER_DETAILS, getString(R.string.jadka_snippet), getContext());
                mCallback.onWidgetContentUpdate();
            }
            if (markerName.equals(PRZYSTAN)) {
                snippet.setText(R.string.lorem_ipsum_detail);
                title.setText(markerName);
                badge.setImageResource(R.drawable.restauracja_przystan);
                Utils.setPreferences(MARKER_NAME, markerName, getContext());
                Utils.setPreferences(MARKER_DETAILS, getString(R.string.przystan_snippet), getContext());
                mCallback.onWidgetContentUpdate();
            }
            if (markerName.equals(KONSPIRA)) {
                snippet.setText(R.string.lorem_ipsum_detail);
                title.setText(markerName);
                badge.setImageResource(R.drawable.restauracja_konspira);
                Utils.setPreferences(MARKER_NAME, markerName, getContext());
                Utils.setPreferences(MARKER_DETAILS, getString(R.string.konspira_snippet), getContext());
                mCallback.onWidgetContentUpdate();
            }
        }
        return rootView;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
