package pl.mdiner.foodbuddy.maps;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import java.util.HashMap;

import pl.mdiner.foodbuddy.R;

import static pl.mdiner.foodbuddy.ui.activities.MainActivity.JADKA;
import static pl.mdiner.foodbuddy.ui.activities.MainActivity.KONSPIRA;
import static pl.mdiner.foodbuddy.ui.activities.MainActivity.PRZYSTAN;

@SuppressLint("Registered")
public class InfoWindowCustom extends AppCompatActivity implements GoogleMap.InfoWindowAdapter  {

    private final HashMap<String, String> markerMap;
    private LayoutInflater inflater;
    private View view;
    private final Context context;

    public InfoWindowCustom(HashMap<String, String> markerMap, Context context) {
        this.markerMap = markerMap;
        this.context = context;
    }
    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        String m = markerMap.get(marker.getId());
        if (m != null) {
            inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final ViewGroup nullParent = null;
            view = inflater.inflate(R.layout.custom_info_contents, nullParent);
            TextView title = view.findViewById(R.id.title);
            TextView snippet = view.findViewById(R.id.snippet);
            ImageView badge = view.findViewById(R.id.badge);
            if (m.equals(JADKA)) {
                title.setText(m);
                snippet.setText(R.string.jadka_snippet);
                badge.setImageResource(R.drawable.restauracja_jadka);
            }
            if (m.equals(PRZYSTAN)) {
                title.setText(m);
                snippet.setText(R.string.przystan_snippet);
                badge.setImageResource(R.drawable.restauracja_przystan);
            }
            if (m.equals(KONSPIRA)){
                title.setText(m);
                snippet.setText(R.string.konspira_snippet);
                badge.setImageResource(R.drawable.restauracja_konspira);
           }
            return view;
        }
        return view;
    }
}